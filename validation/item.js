const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateItemInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.price = !isEmpty(data.price) ? data.price : "";
  data.quantity = !isEmpty(data.quantity) ? data.quantity : "";

   if (Validator.isEmpty(data.name)) {
    errors.name = "Text field is Required";
  }

  if (Validator.isEmpty(data.quantity)) {
    errors.quantity = "You must enter a quantity";
  }

  if (!Validator.isNumeric(data.quantity)) {
    errors.quantity = "Quantity can only be numbers";
  }

  if (Validator.isEmpty(data.price)) {
    errors.price = "Price is required";
  }

  if (!Validator.isNumeric(data.price)) {
    errors.price = "Price can only be numbers";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
