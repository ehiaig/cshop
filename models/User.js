const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Creating the schema
const UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  user_type: {
        type: String,
        enum : ['admin','visitor'],
        default: 'visitor'
    },
  date: { type: Date, default: Date.now }
});

module.exports = User = mongoose.model("users", UserSchema);
