const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Creating the schema
const OrderSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  total: {
    type:Number
  },
  items: [
    {
      name: {
        type: String,
        required: true
      },
      description: {
        type: String
      },
      price: {
        type: String,
        required: true
      },
      quantity: {
      	type:Number,
      	required: true
      }
    }
  ],
  date: { type: Date, default: Date.now }
});

module.exports = Order = mongoose.model("orders", OrderSchema);
