const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Creating the schema



const ProductSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  name: { type: String, required: true },
  description: { type: String },
  price: { type: Number, required: true },
  quantity: { type:Number, required: true },
  date: { type: Date, default: Date.now }
});


module.exports = Product = mongoose.model("products", ProductSchema);
