const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

//Load User Model
const Product = require("../../models/Product");

//Load input Validation
const validateItemInput = require("../../validation/item");

router.get("/test", (req, res) => res.json({ msg: "Product works" }));

//@route GET api/products/all
//@desc Get all products
//@acccess is Public
router.get("/all", (req, res) => {
  const errors = {};

  Product.find()
    .populate("user", ["name", "price", "quantity", "description"])
    .then(products => {
      if (!products) {
        errors.noproduct = "There are no products";
        return res.status(404).json(errors);
      }
      res.json(products);
    })
    .catch(err => res.status(404).json({ products: "There are no products" }));
});

//@route POST api/products
//@desc Create a new product
//@acccess is Private
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {

    //Get the fields in the product
    const newProduct = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
      user: req.user.id
    });
    newProduct.save().then(product => res.json(product));
  }
);

//@route GET api/product
//@desc Get one product
//@acccess is Public
router.get("/", (req, res) => {
  Product.find()
    .sort({ date: -1 })
    .then(products => res.json(products))
    .catch(err => res.status(404).json({ Noproductfound: "No product found" }));
});

//@route GET api/products/:id
//@desc Get product by id
//@acccess is Public
router.get("/:id", (req, res) => {
  Product.findById(req.params.id)
    .then(product => res.json(product))
    .catch(err =>
      res.status(404).json({ Noproductfound: "No product found with that ID" })
    );
});

//@route DELETE api/products/:post_id
//@desc Delete product
//@acccess is Private
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
      Product.findById(req.params.id)
        .then(product => {
          //Check for product owner
          if (product.user.toString() !== req.user.id) {
            return res.status(401).json({
              Unauthorized: "User not authorized to delete this product"
            });
          }
          //Delete product
          product.remove().then(() => res.json({ success: true }));
        })
        .catch(err => res.status(404).json({ productNotFound: "No product found" }));
    });
 
 module.exports = router;