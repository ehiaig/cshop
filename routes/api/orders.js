const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

//Load User Model
const Order = require("../../models/Order");

//Load input Validation
const validateItemInput = require("../../validation/item");

router.get("/test", (req, res) => res.json({ msg: "Order works" }));

//@route GET api/orders/all
//@desc Get all orders
//@acccess is Public
router.get("/all", (req, res) => {
  const errors = {};

  Order.find()
    .populate("user", ["total"])
    .then(orders => {
      if (!orders) {
        errors.noorder = "There are no orders";
        return res.status(404).json(errors);
      }
      res.json(orders);
    })
    .catch(err => res.status(404).json({ orders: "There are no orders" }));
});

//@route POST api/orders
//@desc Create a new order
//@acccess is Private
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {

    //Get the fields in the order
    const newOrder = new Order({
      total: req.body.total,
      user: req.user.id
    });

    newOrder.save().then(order => res.json(order));

  }
);

//@route GET api/order
//@desc Get one order
//@acccess is Public
router.get("/", (req, res) => {
  Order.find()
    .sort({ date: -1 })
    .then(orders => res.json(orders))
    .catch(err => res.status(404).json({ Noorderfound: "No order found" }));
});

//@route GET api/orders/:id
//@desc Get order by id
//@acccess is Public
router.get("/:id", (req, res) => {
  Order.findById(req.params.id)
    .then(order => res.json(order))
    .catch(err =>
      res.status(404).json({ Noorderfound: "No order found with that ID" })
    );
});

//@route POST api/orders/item
//@desc Add items to an order
//@acccess is Private
router.post(
  "/item/:order_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateItemInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Order.findById(req.params.order_id ).then(order => {
      const newItem = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        quantity: req.body.quantity
      };

      //Add it to the items array
      order.items.unshift(newItem);
      order.save().then(order => res.json(order));
    })
    .catch(err =>
        res.status(404).json({ orderNotFound: "Order doesn't exist" })
      );
  }
);


//@route DELETE api/orders/item/order_id/:item_id
//@desc Delete item from an order
//@acccess is Private
router.delete(
  "/item/order_id/:item_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Order.findById( req.params.order_id )
      .then(order => {

        //Check to see if the item exists
        if (
          order.items.filter(
            item => item._id.toString() === req.params.item_id
          ).length === 0
        ) {
          return res
            .status(404)
            .json({ itemNotexist: "item does not exist" });
        }

        //Get index of the item to be removed
        const removeItemIndex = order.items
          .map(item => item._id.toString())
          .indexOf(req.params.item_id);

        //Splice out of array
        order.items.splice(removeItemIndex, 1);

        //Save
        order.save().then(order => res.json(order));
      })
      .catch(err =>
        res.status(404).json({ orderNotFound: "order doesn't exist" })
      );
  }
);

module.exports = router;