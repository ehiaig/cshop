import { ADD_ORDER , GET_ORDERS, GET_ORDER, ORDER_LOADING} from '../actions/types';
const initialState = {
	order: {},
	orders: [],
	loading: false,
}

export default function(state = initialState, action){
	switch(action.type){
		case ORDER_LOADING:
			return {
				...state, 
				loading:true,
			};
		case GET_ORDERS:
			return {
				...state,
				orders:action.payload,
				loading: false,
			};
		case GET_ORDER:
			return {
				...state,
				orders:action.payload,
				loading: false,
			};
		case ADD_ORDER:
			return {
				...state,
				orders: [action.payload, ...state.orders]
			};
		default:
			return state;
	}
}