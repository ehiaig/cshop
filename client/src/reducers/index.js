import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import orderReducer from './orderReducer';
import productReducer from './productReducer';

export default combineReducers({
	auth: authReducer,
	errors: errorReducer,
	order: orderReducer,
	product: productReducer,
});