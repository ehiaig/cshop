import axios from 'axios';

import { ADD_PRODUCT, GET_ERRORS, GET_PRODUCTS, GET_PRODUCT, PRODUCT_LOADING } from './types';

//Add product
export const addProduct = productData => dispatch => {
	axios.post('/api/products', productData)
	.then(res => dispatch({
		type: ADD_PRODUCT,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_ERRORS,
		payload: err.response.data
	}));
};

//Get products
export const getProducts = () => dispatch => {
	dispatch(setProductLoading());
	axios.get('/api/products')
	.then(res => dispatch({
		type: GET_PRODUCTS,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_PRODUCTS,
		payload: null
	}));
};


//Get product by ID
export const getProduct = (id) => dispatch => {
	dispatch(setProductLoading());
	axios.get('/api/products/${id}')
	.then(res => dispatch({
		type: GET_PRODUCT,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_PRODUCT,
		payload: null
	}));
};

//Set Loading state
export const setProductLoading = () => {
	return{
		type: PRODUCT_LOADING
	}
}