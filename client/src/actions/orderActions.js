import axios from 'axios';

import { ADD_ORDER, GET_ERRORS, GET_ORDERS, GET_ORDER, ORDER_LOADING } from './types';

//Add Order
export const addOrder = orderData => dispatch => {
	axios.post('/api/orders', orderData)
	.then(res => dispatch({
		type: ADD_ORDER,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_ERRORS,
		payload: err.response.data
	}));
};

//Get Orders
export const getOrders = () => dispatch => {
	dispatch(setOrderLoading());
	axios.get('/api/orders')
	.then(res => dispatch({
		type: GET_ORDERS,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_ORDERS,
		payload: null
	}));
};


//Get Order by ID
export const getOrder = (id) => dispatch => {
	dispatch(setOrderLoading());
	axios.get('/api/orders/${id}')
	.then(res => dispatch({
		type: GET_ORDER,
		payload: res.data
	})).catch(err => dispatch({
		type:GET_ORDER,
		payload: null
	}));
};

//Set Loading state
export const setOrderLoading = () => {
	return{
		type: ORDER_LOADING
	}
}