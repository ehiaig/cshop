import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';

class Register extends Component {

	constructor(){
		super();
		this.state = {
			name: '',
			email: '',
			password: '',
			confirm_password: '',
			errors: {}
		};

		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentDidMount(){
		if(this.props.auth.isAuthenticated){
			this.props.history.push('/products');
		}
	}

	componentWillReceiveProps(nextProps){
		if(nextProps.errors){
			this.setState({errors: nextProps.errors});
		}
	}

	onChange(e){
		this.setState({[e.target.name]:e.target.value});
	}

	onSubmit(e){
		e.preventDefault();

		const newUser = {
			name: this.state.name,
			email: this.state.email,
			password: this.state.password,
			confirm_password: this.state.confirm_password,

		};

		this.props.registerUser(newUser, this.props.history);
		
	}
  render() {

  	const { errors } = this.state;

    return (
    	<div className="container">
    		<div className="box-shadow p-5 mt-5 rounded-top rounded-bottom">
                <div className="cs-comment-form ">
                    <h4 className="mb-3">Register</h4>
                    <form noValidate onSubmit={ this.onSubmit}>
                    	<TextFieldGroup 
                    		placeholder="Name"
                    		name="name"
                    		type="text"
                    		value={this.state.name} 
                    		onChange={this.onChange}
                    		error={errors.name}
                    	/>
                    	<TextFieldGroup 
                    		placeholder="Email Address"
                    		name="email"
                    		type="email"
                    		value={this.state.email} 
                    		onChange={this.onChange}
                    		error={errors.email}
                    	/>

                    	<TextFieldGroup 
                    		placeholder="Password"
                    		name="password"
                    		type="password"
                    		value={this.state.password} 
                    		onChange={this.onChange}
                    		error={errors.password}
                    	/>

                    	<TextFieldGroup 
                    		placeholder="Confirm Password"
                    		name="confirm_password"
                    		type="password"
                    		value={this.state.confirm_password} 
                    		onChange={this.onChange}
                    		error={errors.confirm_password}
                    	/>
			            <div className="form-group">
			                <button type="submit" className="btn btn-primary btn-block">Signup for Free</button>
			            </div>
			        </form>
                </div>
            </div>
        </div>
    );
  }
}

Register.propTypes = {
	registerUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) =>({
	auth: state.auth,
	errors: state.errors
});
export default connect(mapStateToProps, { registerUser })(withRouter(Register));
