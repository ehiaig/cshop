import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const TextFieldGroup = ({
	name,
	placeholder,
	label,
	error,
	info,
	type,
	value,
	onChange,
	disabled
}) => {
	return(
		<div className="form-group  uiIcon">
	        <input 
	        	type={type} 
	        	className={classnames('input form-control', {'is-invalid':error})}
	        	name={name}
	        	placeholder={placeholder}
	        	value={value} 
	        	onChange={onChange} 
	        	disabled={disabled}
	        	/>
	        	{info && <small className="form-text text-muted">{info}</small>}
	    	{error && (<div className="invalid-feedback">{error}</div>)}
	    </div>
	);
};

TextFieldGroup.propTypes = {
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	info: PropTypes.string,
	type: PropTypes.string.isRequired,
	error: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	disabled: PropTypes.string,
}

TextFieldGroup.defaultProps = {
	type: 'text',
	type: 'hidden'
}
export default TextFieldGroup; 