import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import OrderForm from './OrderForm';
import Spinner from '../common/Spinner';
import OrderFeed from  './OrderFeed';
import { getOrders } from '../../actions/orderActions';


class Orders extends Component{
	componentDidMount(){
		this.props.getOrders();
	}

	render(){
		const { orders, loading } = this.props.order;
		let orderContent;

		if(orders === null || loading){
			orderContent = <Spinner /> 
		}else {
			orderContent = <OrderFeed  orders = {orders} />
		}
		return(
			<div className="orders">
				<div className="container">
					<div className="row">
						<OrderForm />
						{orderContent}
					</div>
				</div>
			</div>
		)
	}
}

Orders.propTypes = {
	getOrders: PropTypes.func.isRequired,
	order: PropTypes.object.isRequired
}
const mapStateToProps = state =>({
	order: state.order
});
export default connect(mapStateToProps, {getOrders})(Orders);