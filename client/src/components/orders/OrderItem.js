import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import OrderFeed from './OrderFeed';
import Spinner from '../common/Spinner';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';

class OrderItem extends Component {

	constructor(props){
		super(props);
		this.state = {
			name: '',
			price: '',
			quantity: '',
			description: '',
			errors: {},
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentWillReceiveProps(newProps){
		if(newProps.errors){
			this.setState({errors: newProps.errors});
		}
	}

	onChange(e){
		this.setState({[e.target.name]:e.target.value});
	}

	onSubmit(e){
		e.preventDefault();

		const { user } = this.props.auth;

		const newItem = {
			total: this.state.total,
		};

		this.props.addOrder(newItem);
		this.setState({total: '0'});
	}

	onDeleteClick(id){
		console.log(id);
	}

	render(){
		const { order, auth } = this.props;
		const { errors } = this.state;

		return (
			<div className="bg-gray py-80 no-header">
		        <div className="container">
		            <div className="cs-row row">
		                <div className="col-md-8">
		                    <div className="cs-blog-view">
		                        <div className="box-shadow p-5 rounded-top">
		                            <div className="cs-blog-content">
		                                <h2 className="cs-blog-title text-primary">Click on the Add Items below to continue your order.</h2>

		                                <Link to={'/order/${order._id}'} className="btn btn-primary btn-shadow"> 
		                                	Add Item
		                                </Link>
		                                {order.user === auth.user.id ? (
		                                	<button 
		                                		onClick={this.onDeleteClick.bind(this, order._id)} 
		                                		type="button" 
		                                		className="btn btn-danger mr-1" >
		                                		<i className="fas fa-times" />
		                                	</button>
		                                ) : null}
		                                
		                            </div>
		                        </div>
		                    </div>
		                </div> 
		            </div>
		        </div> 
		    </div> 
		)
	}
}

OrderItem.propTypes = {
	order: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth:state.auth
});

export default connect(mapStateToProps) (OrderItem);