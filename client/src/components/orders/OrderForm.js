import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import TextFieldGroup from '../common/TextFieldGroup';
import { addOrder } from '../../actions/orderActions';


class OrderForm extends Component{
	constructor(props){
		super(props);
		this.state = {
			total: '',
			errors: {},
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	componentWillReceiveProps(newProps){
		if(newProps.errors){
			this.setState({errors: newProps.errors});
		}
	}

	onChange(e){
		this.setState({[e.target.name]:e.target.value});
	}

	onSubmit(e){
		e.preventDefault();

		const { user } = this.props.auth;

		const newOrder = {
			total: this.state.total,
		};

		this.props.addOrder(newOrder);
		this.setState({total: '0'});
	}
	render(){
		const { errors } = this.state;
		return(
			<div className="call-action-primary py-80">
		        <div className="container">
		            <div className="row">
		                <div className="col-sm-12">
		                    <div className="call-action-primary-content text-center">
		                        <form onSubmit={this.onSubmit} >
		                            <div className="input-group call-action-primary-search">
		                            	<TextFieldGroup 
		                            		placeholder="Total"
				                    		name="total"
				                    		type="hidden"
				                    		value='0' //{this.state.total}
				                    		onChange={this.onChange}
				                    		error={errors.total}
				                    	/>
				                    	<div className="form-group col-sm-12">
                                            <button type="submit" className="btn btn-primary btn-shadow">Create an Order</button>
                                        </div>
		                            </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		)
	}
}

OrderForm.propTypes = {
  addOrder: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) =>({
  auth: state.auth,
  errors: state.errors
});
export default connect(mapStateToProps, { addOrder })(OrderForm);