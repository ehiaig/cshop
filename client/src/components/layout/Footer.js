import React, { Component } from "react";
import { Link } from "react-router-dom";

class Footer extends Component {
  render() {
    return (
      <footer>
        <div className="container">
            <div className="copyright-bar clearfix">
                <div className="row">
                    <div className="col-lg-6">
                        <h1 className="mt-0 mb-0 text-primary">C3Shop</h1>
                    </div>
                    <div className="col-lg-6">
                        <div className="copy-text">
                          Copyright &copy;
                          {new Date().getFullYear()} 2018 C3Shop. 
                          All Right Reserved.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    );
  }
}

export default Footer;
