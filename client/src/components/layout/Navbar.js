import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';

class Navbar extends Component {

  onLogoutClick(e){
    e.preventDefault();
    this.props.logoutUser();
  }

  render() {

    const { isAuthenticated, user } = this.props.auth;
    const authLinks = (
      <ul className="navbar-nav ml-auto">
          <span>{user.name}</span>
         <li className="nav-item">
          <a href="#" onClick={this.onLogoutClick.bind(this)} className="nav-link" >
            Logout
          </a>
        </li>
      </ul>
    );

    const guestLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="btn btn-secondary text-white btn-round btn-nav" to="/register">
            Sign Up
          </Link>
        </li>
        <li className="nav-item"> 
          <Link className="btn btn-primary btn-shadow  text-white btn-round btn-nav" to="/login">
            Login
          </Link>
        </li>
      </ul>
    );

    return (
      <div id="menu_area" className="menu-area fixed-top">
        <nav className="navbar navbar-light navbar-expand-lg mainmenu">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
          </button>
          <Link className="navbar-brand" to="/">
              C3Shop
            </Link>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to="/products">
                      Products
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/orders">
                      Orders
                    </Link>
                  </li>
              </ul>
              {isAuthenticated ? authLinks : guestLinks}
          </div>
      </nav>
      </div>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = (state) =>({
  auth: state.auth
});
export default connect(mapStateToProps, { logoutUser })(Navbar);

