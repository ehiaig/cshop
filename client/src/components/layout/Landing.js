import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';

class Landing extends Component {

  componentDidMount(){
    if(this.props.auth.isAuthenticated){
      this.props.history.push('/dashboard');
    }
  }
  
  render() {
    return (

      <header className="fixed-top-added">
        <div className="container">
            <div className="row">
                <div className="col-sm-6 col-md-7">
                    <div className="hero-content">
                        <h1 className="text-primary">Welcome to the C3Shop website</h1>
                        <p className="text-primary mt-3">Create an account, order items and get sms amd email notifications when you do. It's simple to use.</p>
                        <ul className="pair-btns-list">
                            <li>
                              <Link className="cbtn btn-grad btn-shadow btn-width" to="/register">
                                Get Started
                              </Link>
                            </li>
                            <li> 
                              <Link className="btn btn-secondary text-white btn-round btn-nav" to="/register">
                                Sign Up
                              </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
      
    );
  }
}

Landing.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = (state) =>({
  auth: state.auth
});
export default connect(mapStateToProps, { logoutUser })(Landing);
