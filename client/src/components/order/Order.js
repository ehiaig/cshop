import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../common/Spinner';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import { getOrder } from '../../actions/orderActions';

class Order extends Component {
	render(){

		const { order, auth } = this.props;
		const { errors } = this.state;

		return(
			<div className="bg-gray py-80 no-header">
		        <div className="container">
		            <div className="cs-row row">
		                <div className="col-md-8">
		                    <div className="cs-blog-view">
		                        <div className="box-shadow p-5 mt-5 rounded-top rounded-bottom">
		                            <div className="cs-comment-form ">
		                                <h4 className="mb-3">Add Items</h4>
		                                <form>
		                                	<TextFieldGroup 
					                    		placeholder="Item Name"
					                    		name="name"
					                    		type="text"
					                    		value={this.state.name} 
					                    		onChange={this.onChange}
					                    		error={errors.name}
					                    	/>

					                    	<TextFieldGroup 
					                    		placeholder="Item Price"
					                    		name="price"
					                    		type="number"
					                    		value={this.state.price} 
					                    		onChange={this.onChange}
					                    		error={errors.price}
					                    	/>

					                    	<TextFieldGroup 
					                    		placeholder="Quantity"
					                    		name="quantity"
					                    		type="number"
					                    		value={this.state.quantity} 
					                    		onChange={this.onChange}
					                    		error={errors.quantity}
					                    	/>

					                    	<TextAreaFieldGroup 
					                    		placeholder="Description"
					                    		name="description"
					                    		type="text"
					                    		value={this.state.description} 
					                    		onChange={this.onChange}
					                    		error={errors.description}
					                    	/>
	                                        <div className="form-group col-sm-12">
	                                            <button type="submit" className="btn btn-primary btn-shadow">Submit</button>
	                                        </div>
		                                </form>
		                            </div>
		                        </div>
		                    </div>
		                </div> 
		            </div>
		        </div> 
		    </div> 
		)
	}
}

Order.propTypes = {
	getOrder: PropTypes.func.isRequired,
	order: PropTypes.object.isRequired
}
const mapStateToProps = state =>({
	order: state.order
});

export default connect(mapStateToProps, {getOrder})(Order);