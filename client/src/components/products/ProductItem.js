import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../common/Spinner';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

class ProductItem extends Component {

	render(){
		const { product, auth } = this.props;

		return (
			<div className="bg-gray py-80 no-header">
		        <div className="container">
		            <div className="cs-row row">
		                <div className="col-md-12">
		                    <div className="cs-blog-view">
		                        <div className="box-shadow p-5 rounded-top">
		                            <div className="cs-blog-content">
		                            	<p className="text-center">{product.name}</p>
		                            	<p>{product.description}</p>
		                            	<p>{product.name}</p>
		                            	{product.user === auth.user.id ? (
	                                	<button type="submit" 
	                                		className="btn btn-primary" >Send
	                                	</button>
	                                ) : null}
		                            </div>		                            
		                        </div>
		                    </div>
		                </div> 
		            </div>
		        </div> 
		    </div> 
		)
	}
}

ProductItem.propTypes = {
	product: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired

};

const mapStateToProps = (state) => ({
	auth:state.auth
});

export default connect(mapStateToProps) (ProductItem);