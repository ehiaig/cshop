import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';
import { connect } from 'react-redux';
import Spinner from '../common/Spinner';

class ProductFeed extends Component {
	render(){
		const { products } = this.props;
		const { user } = this.props.auth;

		return products.map(product => <ProductItem key={product._id} product={product}                
		  /> 
		 )
	}
}

ProductFeed.propTypes = {
	products: PropTypes.array.isRequired,
	auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth:state.auth
});

export default connect(mapStateToProps) (ProductFeed);