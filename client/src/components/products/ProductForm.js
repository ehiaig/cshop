import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import { addProduct } from '../../actions/productActions';


class ProductForm extends Component{
	constructor(props){
		super(props);
		this.state = {
			name: '',
			price: '',
			quantity: '',
			description: '',
			errors: {},
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	componentWillReceiveProps(newProps){
		if(newProps.errors){
			this.setState({errors: newProps.errors});
		}
	}

	onChange(e){
		this.setState({[e.target.name]:e.target.value});
	}

	onSubmit(e){
		e.preventDefault();

		const { user } = this.props.auth;

		const newProduct = {
			name: this.state.name,
			price: this.state.price,
			quantity: this.state.quantity,
			description: this.state.description,

		};

		this.props.addProduct(newProduct);
		//clear the fields 
		this.setState({name: ''});
		this.setState({price: ''});
		this.setState({quantity: ''});
		this.setState({description: ''});
	}
	render(){
		const { errors } = this.state;
		return(
			<div className="call-action-primary py-80">
		        <div className="container">
		            <div className="row">
		                <div className="col-sm-12">
		                    <div className="call-action-primary-content text-center">
		                        <form onSubmit={this.onSubmit} >
		                            <div className="input-group call-action-primary-search">
		                            	<TextFieldGroup 
					                    		placeholder="Product Name"
					                    		name="name"
					                    		type="text"
					                    		value={this.state.name} 
					                    		onChange={this.onChange}
					                    		error={errors.name}
					                    	/>

					                    	<TextFieldGroup 
					                    		placeholder="Product Price"
					                    		name="price"
					                    		type="number"
					                    		value={this.state.price} 
					                    		onChange={this.onChange}
					                    		error={errors.price}
					                    	/>

					                    	<TextFieldGroup 
					                    		placeholder="Quantity"
					                    		name="quantity"
					                    		type="number"
					                    		value={this.state.quantity} 
					                    		onChange={this.onChange}
					                    		error={errors.quantity}
					                    	/>

					                    	<TextAreaFieldGroup 
					                    		placeholder="Description"
					                    		name="description"
					                    		type="text"
					                    		value={this.state.description} 
					                    		onChange={this.onChange}
					                    		error={errors.description}
					                    	/>
				                    	<div className="form-group col-sm-12">
                                            <button type="submit" className="btn btn-primary btn-shadow">Create a Product</button>
                                        </div>
		                            </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		)
	}
}

ProductForm.propTypes = {
  addProduct: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) =>({
  auth: state.auth,
  errors: state.errors
});
export default connect(mapStateToProps, { addProduct })(ProductForm);