import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import setAuthToken from './utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import { setCurrentUser, logoutUser } from './actions/authActions';
import PrivateRoute from './components/common/PrivateRoute';
import { Provider } from 'react-redux';
import store from "./store";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Orders from "./components/orders/Orders";
import Order from "./components/order/Order";
import Products from "./components/products/Products";
import Product from "./components/product/Product";
import './App.css';

//Check for token
if(localStorage.jwtToken){

  //Set auth token header auth
  setAuthToken(localStorage.jwtToken);

  //Decode token to get user details
  const decoded = jwt_decode(localStorage.jwtToken);

  //Set current user that is authenticated
  store.dispatch(setCurrentUser(decoded));

  //Check for expired token
  const currentTime = Date.now()/1000;
  if(decoded.exp < currentTime){
    //Logout user
    store.dispatch(logoutUser());
    //TODO:clear current orders

    //Redirect  to login
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store = { store }>
      <Router>
      <div className="App">
      <Navbar />
        <Route exact path="/" component= { Landing } />
        <div className="container">
            <Route exact path="/register" component={ Register } />
            <Route exact path="/login" component={ Login } />

            <Switch>
              <PrivateRoute exact path="/orders" component = {Orders} />
            </Switch>

            <Switch>
              <PrivateRoute exact path="/order/:id" component = {Order} />
            </Switch>

            <Switch>
              <PrivateRoute exact path="/products" component = {Products} />
            </Switch>

            <Switch>
              <PrivateRoute exact path="/product/:id" component = {Product} />
            </Switch>
            
          </div>
      <Footer />
      </div>
      </Router>
      </Provider>
    );
  }
}

export default App;
