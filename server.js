const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require('path')

const users = require("./routes/api/users");
const orders = require("./routes/api/orders");
const products = require("./routes/api/products");

const app = express();
//Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//DB config
const db = require("./config/keys").mongoURI;

//Connect to MongoDB
mongoose
  .connect(db)
  .then(() => console.log("MongoDb Connected"))
  .catch(err => console.log(err));

//Passport Middleware
app.use(passport.initialize());

//Passport Configuration
require("./config/passport")(passport);

//Routes to use
app.use("/api/users", users);
app.use("/api/orders", orders);
app.use("/api/products", products);

//serve static assets if in production
if(process.env.NODE_ENV === 'production'){
	app.use(express.static('client/build'));

	app.get('*', (req, res)=>{
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server running on port ${port}`));